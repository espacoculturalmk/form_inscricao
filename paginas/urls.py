from django.urls import path
from .views import PaginaInicial, Saudacao

urlpatterns = [

    path('', PaginaInicial.as_view(), name='index'),
    path('saudacao/', Saudacao.as_view(), name='saudacao'),

]
