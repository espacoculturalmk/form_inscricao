#from django.shortcuts import render

from .models import Inscrito

from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy

class InscritoCreate(CreateView):

    model = Inscrito
    fields = '__all__'
    template_name = 'paginas/form.html'
    success_url = reverse_lazy('saudacao')

class InscritoList(ListView):

    model = Inscrito
    template_name = 'inscricao/listas/inscritos.html'
