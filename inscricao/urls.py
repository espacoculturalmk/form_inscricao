from django.urls import path
from . import views

from .views import InscritoCreate, InscritoList

urlpatterns = [
    path('inscricao/', InscritoCreate.as_view(), name='inscricao'),

    path('listar/inscritos', InscritoList.as_view(), name='listar-inscritos')
]
