from django.db import models

from django.db import models

id_genero = (
('1', 'Mulhere Cis'),
('2', 'Mulhere Trans'),
('3', 'Homem Cis'),
('4', 'Homem Trans'),
('5', 'Não Binário'),
('99', 'Outro'),
)

sexo = (
('F', 'Feminino'),
('M', 'Masculino'),
('99', 'Outro' ),
)

cor = (
('1', 'Preta'),
('2', 'Parda'),
('3', 'Indígena'),
('4', 'Branca'),
('5', 'Amarela'),
('99', 'Outra'),

)

nacional = (
('1', 'Brasileira(o)'),
('99', 'Outra'),
)

estado = (
("Sao Paulo","São Paulo"),
("Acre","Acre"),
("Alagoas","Alagoas"),
("Amapa","Amapá"),
("Amazonas","Amazonas"),
("Bahia","Bahia"),
("Ceara","Ceará"),
("Distrito Federal","Distrito Federal"),
("Espirito Santo","Espírito Santo"),
("Goias","Goiás"),
("Maranhao","Maranhão"),
("Mato Grosso","Mato Grosso"),
("Mato Grosso do Sul","Mato Grosso do Sul"),
("Minas Gerais","Minas Gerais"),
("Para","Pará"),
("Paraiba","Paraíba"),
("Parana","Paraná"),
("Pernambuco","Pernambuco"),
("Piaui","Piauí"),
("Rio de Janeiro","Rio de Janeiro"),
("Rio Grande do Norte","Rio Grande do Norte"),
("Rio Grande do Sul","Rio Grande do Sul"),
("Rondonia","Rondônia"),
("Roraima","Roraima"),
("Santa Catarina","Santa Catarina"),
("Sergipe","Sergipe"),
("Tocantins","Tocantins"),
)

escolaridade = (
("1","Não Frequentou ensino formal"),
("2","Fundamental"),
("3", "Médio"),
("4","Faculdade"),
("5","Pós-Graduação"),
)

fonte = (

('1',"Compartilhamento em Redes Sociais"),
('2',"Whatsapp"),
('3',"Página do Espaço no Face"),
('4',"Mídia impressa"),
('5',"Amigos ou familiares"),
('6',"Pessoas da sua área de atuação"),
('7',"Mídia tradicional (jornais, revistas, etc)"),
('99',"Outros"),
)

pcd = (
('1',"Sim"),
('2',"Não"),

)

renda = (
('0',"Sem renda"),
('1',"Até R$ 768,00"),
('2', "De R$ 768,01 a R$ 1.063,99"),
('3', "De R$ 1.064,00 a R$ 4.591,00"),
('4', "Acima de R$ 4.591,00"),

)

trabalho = (

('1',"CLT/Regstrado"),
('2', "Autônomo"),
('3', "Não"),
)

atividade = (

('1',"Dança Sênior: profª Yasue, segunda (9h-10h)"),
('2', "Capoeira de Angola: Treinel Luiz Poeira, segunda (20h-21h30)"),
('3', "Tai Chi Pai Lin: profª Olga, terça (8h30-9h30)"),
('4',"Oficinas de Escritas para Mulheres Negras, Indígenas e Periféricas: Oficineira Marli de Fátima Aguiar, terça (19h-21h30)"),
('5', "Ginástica e Alongamento (Sem Vagas): Profª Sabrina Lopes, quarta (8h30-9h30)"),
('6', "Aulas de Ajutes e conserto de roupas: Profª Maria de Lourdes, quarta (14h-16h)"),
('7',"Oficina de Pintura em Tecido e Crochê: Profª Stel e Profª Geni, quarta (14h-16h)"),
('8', "Oficina de Confecção de Flores: profª Benê, quarta (14h-16h)"),
('9', "Solta Dança: Profª Mariah Movimenta, quinta (19h-20h)"),
('10', "Maracatu com Baque do Monte: Prof. Léo Jubarte , quinta (19h-20h)"),

('11',"Yoga: profª e Terapauta Mariana de Paulo, sexta (8h30-9h30)"),
('12', "PAI - Programa de Atendimento ao Idoso, sexta (13h-16h)"),
('13', "Zumba: profª Maísa, sexta (19h-20h)"),

('14',"Horta Comunitária: Mutirão, sábado (a partir das 8h30)"),
('15', "Cursinho Popular Emancipa, sábado (a partir das 9h)"),

('16', "Reciclaem com o Coletivo Recicla Vida, sábado (15h-17h)"),
('17',"Capoeira de Angola: Treinel Luiz Poeira, sábado (16h30-18h)"),
('18', "Encontro das Mulheres da Comunidade Vila Sônia, sábado (18h)"),
('19', "Feira Agroecológica - Comida de verdade direto da roça, sábado (Confira as próximas em nossas redes sociais 10h-14h)"),

)

class Inscrito(models.Model):

    atividade = models.CharField(max_length=2, choices=atividade, verbose_name="Atividade/Oficina/Aula: ", help_text = 'Escolha a atividade que deseja participar')

    nome = models.CharField(max_length=100, verbose_name="Nome")
    data_nascimento = models.DateField()
    cpf = models.CharField(unique=True, max_length=11, verbose_name="CPF")

    sexo = models.CharField(max_length=2, choices=sexo)
    id_genero = models.CharField(max_length=10, choices=id_genero, verbose_name="Identidade de Gênero", help_text='Você se identifica com qual gênero?')
    nacionalidade = models.CharField(max_length=9, choices=nacional)
    cor = models.CharField(max_length=9, choices=cor)
    ddd = models.CharField(max_length=2, verbose_name="DDD", default='11')
    telefone = models.CharField(unique=True,max_length=9)

    ## endereço
    logradouro = models.CharField(max_length=200, help_text='Nome da rua, ou avenida, etc onde está localizada a sua residência')
    num_res = models.CharField(max_length=9999, verbose_name='Número', help_text='Número da sua residência')
    bairro = models.CharField(max_length=60)
    cidade = models.CharField(max_length=40)
    estado = models.CharField(max_length=19, choices=estado, default='Sao Paulo')
    cep = models.CharField(max_length=7, verbose_name='CEP', help_text='sem números, sem traços')

    ## Formacao

    esclaridade = models.CharField(max_length=9, choices=escolaridade, help_text='Você frequentou escola?')
    instituicao = models.CharField(max_length=40, verbose_name='Nome da escola/faculdade')
    data_conclusao = models.DateField(verbose_name='Data de conclusão dos estudos', help_text='data aproximada de quando você terminou ou parou de estudar')

    ## Outras infos

    fonte = models.CharField(max_length=9, choices = fonte, verbose_name='Como você ficou sabendo da gente?')
    pcd = models.CharField(max_length=9, choices=pcd, default='2', verbose_name='Pessoa com deficiência', help_text='Você tem alguma diferença que requer cuidado?')
    renda = models.CharField(max_length=9, choices=renda, help_text='Qual é a sua renda aproximadamente')
    trabalha = models.CharField(max_length=9, choices=trabalho, verbose_name='Emprego', help_text='Você está empregado/a?')
    responsavel = models.CharField(max_length=40, blank=True, null=True, verbose_name='Responsável (caso voê seja menor de idade)',help_text='Nome do seu pai ou da sua mãe')
    cpf_responsavel = models.CharField(max_length=11, blank=True, null=True, verbose_name='CPF da pessoa responsável')

    def __str__(self):
        #return self.nome
        return "{} {()}".format(self.pk, self.nome)
